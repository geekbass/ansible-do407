# Study Stuff for Ansible Exam DO407
This repo is used as a study guide for exam [DO407](https://www.redhat.com/en/services/training/ex407-red-hat-certified-specialist-in-ansible-automation-exam) for Ansible. The following topics need to be covered:

- Understand core components of Ansible
    - [Inventories](#inventory)
    - [Modules](#modules)
    - [Variables](#variables)
    - [Facts](#facts)
    - [Plays](#plays)
    - [Playbooks](#playbooks)
    - [Configuration files](#configuration-files)

- Run [ad-hoc](#ad-hoc) Ansible commands
- Use both static and dynamic inventories to define groups of hosts
- Utilize an existing dynamic inventory script
- Create Ansible plays and playbooks
    - Know how to work with commonly used Ansible modules
    - Use variables to retrieve the results of running a commands
    - Use conditionals to control play execution
    - Configure error handling
    - Create playbooks to configure systems to a specified state
    - Selectively run specific tasks in playbooks using tags
- Create and use templates to create customized configuration files
- Work with Ansible variables and facts
- Create and work with [roles](#roles)
- Download roles from an Ansible Galaxy and use them
- Manage parallelism
- Use Ansible Vault in playbooks to protect sensitive data
- Install Ansible Tower and use it to manage systems
- Use provided documentation to look up specific information about Ansible modules and commands


Will provide notes as well as code samples to acheive the above. 

# Lab Setup
I am currently switching back and forth between my local Mac and some servers that I can create with my [LinuxAcademy](https://linuxacademy.com) account. 

After you create these servers (or for any machines you wish to manage) you will need to `ssh-copy-id` between all of your managed machines OR disable host key check from ansible (which I do not advise in this scenario). 

NOTE: That when running from Mac, I must specify `--user` that exists on the managed machine for these to work. I am just using the default users created on the L.A. nodes which is `user`. 

To install `ansible` locally on your Mac you can issue through brew.
```
brew install ansible
```

# Study Sources
- Linux Academy's [Red Hat Certified Specialist in Ansible Automation (EX407) Preparation Course](https://linuxacademy.com/linux/training/course/name/linux-academy-red-hat-certified-specialist-in-ansible-automation)

- [RH's Ansible DO407 Course](https://www.redhat.com/en/services/training/do407-automation-ansible-i)

- [Official Ansible Docs](https://docs.ansible.com)

- MORE TO COME

# Notes

### Installation
``` 
# On control Node ONLY
yum install ansible -y

# OR
pip install ansible

# For Mac
brew install ansible
```

### Basic Ansible Commands
`ansible` - ad-hoc commands

`ansible-playbook` - Runs a playbook

`ansible-vault` - Manage ansible encryption

`ansible-galaxy` - Manage roles for galaxy.ansible.com

`ansible-doc` - Documentation for Ansible 

`ansible-pull` - Pull playbooks to servers 

### Inventory
Create a file and place a list of hosts. You will reference this as an argument/override (`-i`, `--inventory` or `--inventory-file`) or specify it in your `ansible.cfg`. You can group nodes together by placing `[ ]` around them. 

```
cat << EOF >> inventory
[control]
wbassler231.mylabserver.com
EOF

# Test the inventory
ansible control -i inventory --list-hosts 
  hosts (1):
    wbassler231.mylabserver.com
```

-There are static (text/.ini) and dynamic (scripted) types of inventory. 

-2 Groups exist no matter what: `all` and `ungrouped`

-Nested Groups: Multiple groups combined together with the `:children` suffix.
```
cat << EOF >> inventory
[usa]
wbassler232.mylabserver.com

[canada]
wbassler233.mylabserver.com

[north-america:children]
canada
usa
EOF

ansible -i inventory -m ping north-america
wbassler232.mylabserver.com | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
wbassler233.mylabserver.com | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```

-You can specify ranges using [START:END]. Example: `wbassler[2:5].com` would include nodes wbassler2.com - wbassler5.com.




For official Docs for [Inventory](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)

### Modules
-The way that Ansible performs tasks.

-Can be ad-hoc or within playbooks.

### Variables
-Variable names should be letters, numbers and underscores.

-Variables should always start with a letter.

-Variables can be defined in the inventory.

-Variables can be defined in a playbook or role and can be referenced by templates.


### Facts 
-Data gathered about systems through the setup module

-`gathering_facts: no option` 

### Plays
-A playbook is made up of individual Plays (tasks). 

### Playbooks
-Written in YAML format and begin with `---`

### Configuration Files
-3 locations for `ansible.cfg` which are searched by Ansible in the following order:
- `./ansible.cfg`
- `~/.ansible.cfg`
- `/etc/ansible/ansible.cfg`

-You can also set the location with the `$ANSIBLE_CONFIG` env variable which will take precedence over all. 

-Sample config assumes that the user has sudo permissions without a password
```
[defaults]
inventory = ./inventory
remote_user = user
ask_pass = false

[privilege_escalation]
become = true
become_method = sudo
become_user = root
become_ask_pass = false
```

-Comments in config files: `#` and `;`

-Example use:
```
cat << EOF >> ansible.cfg
[defaults]
inventory = ./inventory
remote_user = user
ask_pass = false

[privilege_escalation]
become = true
become_method = sudo
become_user = root
become_ask_pass = true
EOF

ansible north-america -m ping
SUDO password:
wbassler232.mylabserver.com | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
wbassler233.mylabserver.com | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```

For official Docs for [Config files](https://docs.ansible.com/ansible/latest/installation_guide/intro_configuration.html)

### Roles
-"Split" up playbooks 

-Great for sharing and reuse

-Directory structure containing all the files, variables, handlers, Jinja templates, and tasks needed to automate a workflow

-[Roles Topic](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html?) - great doc 

-Creating a role:
```
mkdir roles

ansible-galaxy init roles/first-role
- roles/first-role was created successfully

tree roles/
roles/
└── first-role
    ├── README.md
    ├── defaults
    │   └── main.yml
    ├── files
    ├── handlers
    │   └── main.yml
    ├── meta
    │   └── main.yml
    ├── tasks
    │   └── main.yml
    ├── templates
    ├── tests
    │   ├── inventory
    │   └── test.yml
    └── vars
        └── main.yml

9 directories, 8 files
```



### ad-hoc
-Execute Ansible tasks easily and on the fly from the cmd line

```
ansible host-pattern -m module [-a 'module arguments'] [-i inventory]
```

-Use `ansible-doc` for documentation on modules

`ansible-doc -l` - list avail modules

`ansible-doc $module` - see the documentation for the module

-Important Modules: user, ping, command, shell, raw, copy

`ansible --help` - for command line options

-Some examples:
```
ansible -m ping all
SUDO password:
wbassler232.mylabserver.com | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
wbassler233.mylabserver.com | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```

```
ansible -m user -a "name=user state=present" all
SUDO password:
wbassler233.mylabserver.com | SUCCESS => {
    "append": false,
    "changed": false,
    "comment": "",
    "group": 1001,
    "home": "/home/user",
    "move_home": false,
    "name": "user",
    "shell": "/bin/bash",
    "state": "present",
    "uid": 1001
}
wbassler232.mylabserver.com | SUCCESS => {
    "append": false,
    "changed": false,
    "comment": "",
    "group": 1001,
    "home": "/home/user",
    "move_home": false,
    "name": "user",
    "shell": "/bin/bash",
    "state": "present",
    "uid": 1001
}
```

```
ansible -m command -a "id" usa
SUDO password:
wbassler232.mylabserver.com | SUCCESS | rc=0 >>
uid=0(root) gid=0(root) groups=0(root) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
```

```
> ansible -m copy -a 'content="Hello! Welcome!\n" dest=/etc/motd' allSUDO password:
wbassler232.mylabserver.com | SUCCESS => {
    "changed": true,
    "checksum": "0defc4d6909ec332372d577f11c75238aff6e096",
    "dest": "/etc/motd",
    "gid": 0,
    "group": "root",
    "md5sum": "fb472c22c71e5ab2cc538279184a92d8",
    "mode": "0644",
    "owner": "root",
    "secontext": "system_u:object_r:etc_t:s0",
    "size": 16,
    "src": "/home/user/.ansible/tmp/ansible-tmp-1544586787.54-34962151465940/source",
    "state": "file",
    "uid": 0
}
wbassler233.mylabserver.com | SUCCESS => {
    "changed": true,
    "checksum": "0defc4d6909ec332372d577f11c75238aff6e096",
    "dest": "/etc/motd",
    "gid": 0,
    "group": "root",
    "md5sum": "fb472c22c71e5ab2cc538279184a92d8",
    "mode": "0644",
    "owner": "root",
    "secontext": "system_u:object_r:etc_t:s0",
    "size": 16,
    "src": "/home/user/.ansible/tmp/ansible-tmp-1544586787.53-245608216953541/source",
    "state": "file",
    "uid": 0
}
``` 

```
ansible -m shell -a "cat /etc/motd" all
SUDO password:
wbassler232.mylabserver.com | SUCCESS | rc=0 >>
Hello! Welcome!

wbassler233.mylabserver.com | SUCCESS | rc=0 >>
Hello! Welcome!
```